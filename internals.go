package trix

import (
	"fmt"
	"time"
)

// StringValue return the node's value as a string
func (node *Node) StringValue() string {
	if node == nil || node.Value == nil {
		return ""
	} else if s, ok := node.Value.(string); ok {
		return s
	} else if t, ok := node.Value.(time.Time); ok {
		return t.Format(time.RFC3339Nano)
	}
	return fmt.Sprint(node.Value)
}

func internalSet(node *Node, keys []string, value Value) *Node {
	if len(keys) == 0 {
		return nil
	}

	// find the node to update, creating intermediate nodes as necessary
	nodeToUpdate := node
	for _, key := range keys {
		child, found := nodeToUpdate.Children[key]
		if !found {
			child = NewNode(key)
			nodeToUpdate.Adopt(child)
		}

		// continue using this as the parent
		nodeToUpdate = child
	}

	// update the child's value
	if value != nil {
		nodeToUpdate.Value = value
	}
	return nodeToUpdate
}

func internalMerge(node *Node, other *Node) *Node {
	// find the node to update, creating intermediate nodes as necessary
	nodeToUpdate := node.GetRoot()
	for _, key := range other.Path() {
		child, found := nodeToUpdate.Children[key]
		if !found {
			child = NewNode(key)
			nodeToUpdate.Adopt(child)
		}

		// continue using this as the parent
		nodeToUpdate = child
	}

	if other.IsLeaf() {
		nodeToUpdate.Value = other.Value
	} else {
		// add all children
		for _, key := range other.ChildKeys {
			internalMerge(node, other.Children[key])
		}
	}
	return nodeToUpdate
}

// internalUnset will remove the specified node and return it
func internalUnset(node *Node, keys []string) *Node {
	if len(keys) > 0 {
		key, keys := keys[0], keys[1:]
		if child, found := node.Children[key]; found {
			if len(keys) > 0 {
				// this isn't the last key
				return internalUnset(child, keys)
			}

			// remove it from both lists
			delete(node.Children, key)
			for index, ck := range node.ChildKeys {
				if ck == key {
					node.ChildKeys = append(node.ChildKeys[:index], node.ChildKeys[index+1:]...)
					break
				}
			}
			child.Parent = nil
			return child
		}
	}
	return nil
}

// internalGetNodes will return a list of all nodes that match the spec
func internalGetNodes(node *Node, spec []string, limit int) NodeList {
	result := NodeList{}
	internalMatch(node, spec, false, func(n *Node) {
		if limit == 0 || len(result) < limit {
			result = append(result, n)
		}
	})
	return result
}

// internalMatch will filter the node's children to match the keySpec.
// The callback is called for every node that matches the spec.
// If leaves=true, the callback will instead be called for each leaf node under
// the nodes that match the spec.
// Parent scopes will also be searched, but overriden keys will only be returned.
func internalMatch(node *Node, keySpec []string, leaves bool, cb func(*Node)) {
	if len(keySpec) == 0 {
		return
	}

	seen := map[string]bool{}
	var match func(*Node, ...string)
	match = func(node *Node, spec ...string) {
		for _, childKey := range node.ChildKeys {
			if s := spec[0]; s == "*" || childKey == "*" || childKey == s {
				childNode := node.Children[childKey]
				switch {
				case len(spec) > 1:
					// there are still spec keys left to match
					match(childNode, spec[1:]...)

				case !leaves:
					// normal mode, spec matched
					if path := childNode.PathString(); !seen[path] {
						seen[path] = true
						cb(childNode)
					}

				case childNode.IsLeaf():
					// leaves=true, and this is a leaf node
					if path := childNode.PathString(); !seen[path] {
						seen[path] = true
						cb(childNode)
					}

				default:
					// leaves=true, and this is not a leaf node; match all children
					match(childNode, "*")
				}
			}
		}
	}

	for node != nil {
		match(node, keySpec...)

		// is there a parent scope where can also look?
		parentScope := node.GetRoot().Parent
		if parentScope == nil {
			break
		}

		if node.IsRoot() {
			node = parentScope
		} else {
			node = parentScope.GetNode(node.PathString())
		}
	}
}

func internalMatchLeaves(node *Node, keySpec []string, cb func(*Node)) {
	seen := map[string]bool{}
	var match func(*Node, ...string)
	match = func(node *Node, spec ...string) {
		if len(spec) == 0 {
			return
		}
		for _, childKey := range node.ChildKeys {
			if s := spec[0]; s == "*" || childKey == "*" || childKey == s {
				childNode := node.Children[childKey]
				if len(spec) > 1 {
					// still specs keys left to match
					match(childNode, spec[1:]...)
					continue
				}

				// spec fully matched
				if path := childNode.PathString(); !seen[path] {
					seen[path] = true
					cb(childNode)
				}
			}
		}
	}

	for node != nil {
		match(node, keySpec...)

		// is there a parent scope where can also look?
		parentScope := node.GetRoot().Parent
		if parentScope == nil {
			break
		}

		if node.IsRoot() {
			node = parentScope
		} else {
			node = parentScope.GetNode(node.PathString())
		}
	}
}

// internalTryGetNode will try o find the keys starting from the specified node.
func internalTryGetNode(node *Node, parsedKeys []string) (*Node, error) {
	if found := internalGetNodes(node, parsedKeys, 1); len(found) > 0 {
		return found[0], nil
	}
	return nil, errorNodeNotFound
}
