package trix

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
)

// NodeFlag is the type used to associate flags with a node
type NodeFlag byte

const (
	// NoFlags means the node has no flag
	NoFlags NodeFlag = 0

	// ForceMap means the node's direct children will be serialised as a map,
	// even if all keys are numeric
	ForceMap NodeFlag = 1 << (iota - 1)

	// ForceArray means the node's direct children will be serialised as
	// an array, even if some of the keys are not numeric
	ForceArray

	// IsRoot means the node is considered a Root node.
	// That is, `Parent` points to a parent tree, not a parent node.
	IsRoot
)

// Value is the type for a trix node
type Value interface{}

// Node represents a node
type Node struct {
	Key       string
	Value     Value
	Children  map[string]*Node
	ChildKeys []string
	Parent    *Node
	Flags     NodeFlag
}

// NewNode returns the pointer to a new, empty node.
func NewNode(key string) *Node {
	return &Node{
		Key:       key,
		Children:  map[string]*Node{},
		ChildKeys: []string{},
	}
}

// NewRoot returns a new, empty root node.
func NewRoot() *Node {
	root := NewNode("")
	root.Flags = IsRoot
	return root
}

// MustLoad is a convenient method to load
func MustLoad(filename string) *Node {
	root := NewRoot()
	if err := root.GraftFile(filename); err != nil {
		panic(fmt.Errorf("Could not load configuration from %s: %v", filename, err))
	}
	return root
}

// GetRoot returns the root for this node.
func (node *Node) GetRoot() *Node {
	p := node
	for ; p != nil && p.Parent != nil && p.Flags&IsRoot == 0; p = p.Parent {
	}
	return p
}

// Depth returns the depth of the node, that is, the number of parents it has.
// The minimum (root node) depth is 0.
func (node *Node) Depth() int {
	depth := 0
	for n := node; n != nil && n.Parent != nil && !n.IsRoot(); n = n.Parent {
		depth++
	}
	return depth
}

// PathString returns the path up to (and including) this node, as a string.
func (node *Node) PathString() string {
	return strings.Join(node.Path(), ".")
}

// Path returns the path up to (and including) this node, as a string slice.
func (node *Node) Path() []string {
	depth := node.Depth()
	path := make([]string, depth)
	for n := node; n != nil && depth > 0; n = n.Parent {
		depth--
		if n.Key != "" { // root has no key
			path[depth] = n.Key
		}
	}
	return path
}

// With creates a new child root tree, under this node's root.
// This new tree inherits values from the original one, and may override
// values without affecting the original tree.
// If arguments are specified, they are added to this node's path,
// under the new root.
func (node *Node) With(args ...Args) *Node {
	root := node.GetRoot()
	newRoot := NewRoot()
	newRoot.Parent = root

	// if this is not called from the root, a new node should be created
	// to contain the arguments
	argsTarget := newRoot
	if root != node {
		argsTarget = internalSet(newRoot, node.Path(), nil)
	}

	// add all arguments
	for _, arg := range args {
		for key, value := range arg {
			argsTarget.SetKey(key, value)
		}
	}

	return newRoot
}

// FromArgs returns a new root node from an args structure.
func FromArgs(args Args) *Node {
	root := NewRoot()
	for key, value := range args {
		root.SetKey(key, value)
	}
	return root
}

// Rename changes the node's key. It does ensure the parent node is kept sorted.
func (node *Node) Rename(newKey string) *Node {
	if node != nil {
		if parent := node.Parent; parent != nil {
			parent.Unset(node.Key)
			node.Key = newKey
			parent.Adopt(node)
		}
	}
	return node
}

// IsLeaf returns whether the node is a left one (has no children).
func (node *Node) IsLeaf() bool {
	return len(node.ChildKeys) == 0
}

// IsRoot returns whether the node is a root.
func (node *Node) IsRoot() bool {
	return node.Flags&IsRoot != 0
}

// Adopt the new child into the node's children, removing it from the previous
// parent if necessary.
func (node *Node) Adopt(child *Node) {
	if child == nil {
		return
	}

	// sever link with former parent
	if p := child.Parent; p != nil {
		p.Unset(child.Key)
	}

	if other, found := node.Children[child.Key]; found {
		// there's another child with the same key; remove it
		node.Unset(other.Key)
	}

	// add the child, update its parent and depth
	node.Children[child.Key] = child
	node.ChildKeys = append(node.ChildKeys, child.Key)
	child.Parent = node
}

// Graft a new subnode into the current one. Recursively create clones of each
// node as necessary. Any existing nodes that aren't overwritten are kept.
// Return the either newly-created or existing node.
func (node *Node) Graft(original *Node) *Node {
	if original == nil {
		return nil
	}

	// ensure the node exists
	old := node.GetNode(original.Key)
	if old == nil {
		old = NewNode(original.Key)
		old.Parent = node
		node.Adopt(old)
		node.Sort()
	}

	// overwrite the value
	old.Value = original.Value

	// merge children
	for _, key := range original.ChildKeys {
		old.Graft(original.Children[key])
	}

	return old
}

// Merge a new subnode into the current root. This is similar to Graft, but
// the nodes are created under the node's root, under the original node's path.
// It's the same as node.GetRoot().SetKey(n.PathString(), n.Value)
func (node *Node) Merge(n *Node) *Node {
	return internalMerge(node, n)
}

// hasOnlyNumericKeys returns whether the node only has numeric keys
func (node *Node) hasOnlyNumericKeys() bool {
	for _, key := range node.ChildKeys {
		if _, err := strconv.Atoi(key); err != nil {
			return false
		}
	}
	return true
}

// Sort sorts a node's children by their keys.
// Nodes with only integer keys are sorted numerically,
// while others are sorted alphabetically.
func (node *Node) Sort() {
	if node.hasOnlyNumericKeys() {
		NumericStringSlice(node.ChildKeys).Sort()
	} else {
		sort.StringSlice(node.ChildKeys).Sort()
	}
}

// SortRecursively will recursively sorts a node's children by their keys.
// Nodes with only integer keys are sorted numerically,
// while others are sorted alphabetically.
func (node *Node) SortRecursively() {
	node.Sort()
	for _, child := range node.Children {
		if len(child.Children) > 0 {
			child.SortRecursively()
		}
	}
}

// Set a child node with the specified value.
func (node *Node) Set(keys []interface{}, value Value) *Node {
	return internalSet(node, ParseKeys(keys), value)
}

// SetKey sets a child node with the specified value.
func (node *Node) SetKey(key string, value Value) *Node {
	return internalSet(node, ParseKeys([]interface{}{key}), value)
}

// FillKey will, on the first call, set the node's value. On subsequent calls
// it will convert the node from a list to a node, and add additional items.
// more than one value
func (node *Node) FillKey(keys string, value Value) *Node {
	childNode := internalSet(node, ParseKeys([]interface{}{keys}), nil) // get/create the child node
	var newNode *Node
	if len(childNode.ChildKeys) == 0 {
		if childNode.Value == nil {
			// the node has just been created; set its value
			newNode = childNode
		} else {
			// node has a value; convert original value to a child, and push the second one
			childNode.Push().Value = childNode.Value
			childNode.Value = nil
		}
	}
	if newNode == nil {
		newNode = childNode.Push()
	}
	newNode.Value = value
	return newNode
}

// Push adds new child nodes, using a unique number as the next ID.
// This is usefull for filling-in arrays.
// If no value is specified, a single node is created, with nil as the value.
// Otherwise a node is created for each value specified.
// The last created node is returned.
func (node *Node) Push(values ...interface{}) *Node {
	if len(values) == 0 {
		values = []interface{}{nil}
	}

	var last *Node
	id := len(node.ChildKeys)
	for _, value := range values {
		for {
			id++
			sid := fmt.Sprint(id)
			if _, found := node.Children[sid]; !found {
				last = node.SetKey(sid, value)
				break
			}
		}
	}
	return last
}

// PushValues adds all specified values as subnodes, using unique number as IDs.
// Return the original node.
func (node *Node) PushValues(values ...Value) *Node {
	for _, value := range values {
		node.Push().Value = value
	}
	return node
}

// Unset the child with the specified key, and return it.
// If the child is not found, return nil.
func (node *Node) Unset(keys ...interface{}) *Node {
	return internalUnset(node, ParseKeys(keys))
}

// Clear removes all children from the node.
func (node *Node) Clear() {
	// detach children first
	for _, child := range node.Children {
		child.Parent = nil
	}

	node.Children = map[string]*Node{}
	node.ChildKeys = []string{}
}

// HasKey returns whether the node has all the specified keys
func (node *Node) HasKey(key ...string) bool {
	for _, k := range key {
		if _, has := node.Children[k]; !has {
			return false
		}
	}
	return true
}

// HasValue returns whether the node has the specified value
func (node *Node) HasValue(value Value) bool {
	return node.Value == value
}

// Match will run the callback for all leaf nodes that match the spec.
func (node *Node) Match(callback func(*Node), keys ...interface{}) {
	internalMatch(node, ParseKeys(keys), true, callback)
}

// Filter will return a list of all nodes that match the spec.
func (node *Node) Filter(keys ...interface{}) NodeList {
	var matches NodeList
	internalMatch(node, ParseKeys(keys), false, func(node *Node) {
		matches = append(matches, node)
	})
	return matches
}

// Dump writes to stout the node contents in a verbose way.
// This should only be used for debugging.
func (node *Node) Dump() {
	node.dump(0)
}

func (node *Node) dump(depth int) {
	for i := 0; i < depth-1; i++ {
		fmt.Printf("  ")
	}
	if depth > 0 {
		fmt.Printf("`-")
	}
	if node.Key == "" {
		fmt.Printf("<nokey>")
	} else {
		fmt.Printf("%s", node.Key)
	}
	if node.IsLeaf() {
		fmt.Printf("=%v [%T]", node.Value, node.Value)
	}
	var flags []string
	for f, s := range map[NodeFlag]string{ForceMap: "map", ForceArray: "array", IsRoot: "root"} {
		if node.Flags&f > 0 {
			flags = append(flags, s)
		}
	}
	if len(flags) > 0 {
		fmt.Printf(" (%s)", strings.Join(flags, ","))
	}
	fmt.Println()
	for _, k := range node.ChildKeys {
		node.Children[k].dump(depth + 1)
	}
}
