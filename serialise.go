package trix

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
)

// String returns the string representation of a single node.
func (node *Node) String() string {
	if node == nil {
		return ""
	}
	if node.IsRoot() {
		return "<root>"
	}

	path := node.PathString()
	if node.IsLeaf() {
		return path + "=" + node.StringValue()
	}
	return path
}

// StringTree returns the string representation of a node and its descendants.
func (node *Node) StringTree() string {
	if node == nil {
		return ""
	}

	if len(node.ChildKeys) > 0 {
		return node.Filter("*").String()
	}

	return node.String()
}

type tWriter struct {
	buf bytes.Buffer
}

func (w *tWriter) Write(p []byte) (n int, err error) {
	if p[len(p)-1] == '\n' {
		// work around json.Marshal's annoying obsession with '\n's
		return w.buf.Write(p[:len(p)-1])
	}
	return w.buf.Write(p)
}

func (w *tWriter) Bytes() []byte {
	return w.buf.Bytes()
}

func (w *tWriter) WriteByte(c byte) error {
	return w.buf.WriteByte(c)
}

// MarshalJSON returns the node node's and its descendants' representation
// in JSON.
func (node *Node) MarshalJSON() ([]byte, error) {
	if node == nil {
		return []byte{}, nil
	}

	// single value
	forceArray := node.Flags&ForceArray > 0
	forceMap := node.Flags&ForceMap > 0
	if node.IsLeaf() && !forceArray && !forceMap {
		return json.Marshal(node.Value)
	}

	// not a single value, we need a buffer/encoder
	buf := tWriter{}
	enc := json.NewEncoder(&buf)
	enc.SetIndent("", "")
	enc.SetEscapeHTML(false)

	// return a sorted array
	if forceArray || (!forceMap && node.hasOnlyNumericKeys()) {
		children := make([]interface{}, len(node.ChildKeys))
		for index, key := range node.ChildKeys {
			children[index] = node.Children[key]
		}
		err := enc.Encode(children)
		return buf.Bytes(), err
	}

	// serialise children as a sorted map
	buf.Write([]byte{'{'})
	for i, key := range node.ChildKeys {
		if i > 0 {
			buf.WriteByte(',')
		}
		enc.Encode(key)
		buf.Write([]byte{':'})
		enc.Encode(node.Children[key])
	}
	buf.WriteByte('}')
	return buf.Bytes(), nil
}

// AsJSON returns the node and its children as a JSON object
func (node *Node) AsJSON() []byte {
	buf, err := node.MarshalJSON()
	if err != nil {
		buf = nil
	}
	return buf
}

func (nodes NodeList) String() string {
	buf := bytes.Buffer{}
	for _, n := range nodes {
		buf.Write([]byte(n.String()))
		buf.Write([]byte("\n"))
	}
	return buf.String()
}

// First returns the first node from the list, or nil if the list is empty.
func (nodes NodeList) Write(w io.Writer, linePrefix string) {
	if len(nodes) == 0 {
		return
	}

	// create a temporary root here
	tempRoot := NewRoot()
	tempRoot.ChildKeys = make([]string, len(nodes))
	for i, n := range nodes {
		tempRoot.ChildKeys[i] = n.Key
		tempRoot.Children[n.Key] = n
	}
	tempRoot.writeInternal(w, linePrefix)
}

func (node *Node) writeInternal(w io.Writer, linePrefix string) {
	if node.IsLeaf() {
		fmt.Fprintf(w, "%s%s=%s\n",
			linePrefix,
			node.PathString(),
			node.StringValue(),
		)
		return
	}
	for _, k := range node.ChildKeys {
		node.Children[k].writeInternal(w, linePrefix)
	}
}

// Write a textual representation of a node and its descendants.
func (node *Node) Write(w io.Writer, linePrefix string) {
	if node == nil {
		return
	}
	node.writeInternal(w, linePrefix)
}
